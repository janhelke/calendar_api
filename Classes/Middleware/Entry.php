<?php
declare(strict_types=1);

namespace JanHelke\CalendarApi\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Extbase\Object\Exception;

/**
 * Class Entry
 */
class Entry implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws Exception
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        if (strpos($request->getUri()->getPath(), '/api/calendar/entry') === 0) {
            if ($request->getUri()->getPath() == '/api/calendar/entry/create') {
                return $this->createEntry($request);
            }

            if ($request->getUri()->getPath() == '/api/calendar/entry/update') {
                return $this->updateEntry($request);
            }
        }

        return $handler->handle($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Response
     */
    protected function createEntry(ServerRequestInterface $request): Response
    {
        return $this->generateJsonResponse([]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Response
     */
    protected function updateEntry(ServerRequestInterface $request): Response
    {
        return $this->generateJsonResponse([]);
    }

    /**
     * @param array $response
     * @return Response
     */
    protected function generateJsonResponse(array $response): Response
    {
        $body = new Stream('php://temp', 'rw');
        $body->write(json_encode($response));
        return (new Response())
            ->withHeader('content-type', 'application/json; charset=utf-8')
            ->withBody($body)
            ->withStatus(200);
    }
}
