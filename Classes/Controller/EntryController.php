<?php
declare(strict_types=1);

namespace JanHelke\CalendarApi\Controller;

use DateTime;
use JanHelke\CalendarApi\Domain\Repository\EntryRepository;
use JanHelke\CalendarApi\Utility\ApiUtility;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;

/**
 * Calender Api Controller
 */
class EntryController extends ActionController
{
    /**
     * @var JsonView
     */
    protected $view;

    /**
     * @var string
     */
    protected $defaultViewObjectName = JsonView::class;

    protected ?EntryRepository $entryRepository = null;

    /**
     * Resolves and checks the current action method name
     *
     * @return string Method name of the current action
     */
    protected function resolveActionMethodName(): string
    {
        $actionName = '';
        switch ($this->request->getMethod()) {
            case 'HEAD':
            case 'GET':
                $actionName = ApiUtility::guessActionMethodNameBasedOnRequest($this->request);
                break;
            case 'POST':
            case 'PUT':
            case 'DELETE':
            default:
                $this->throwStatus(400, null, 'Bad Request.');
        }

        return $actionName;
    }

    public function findEventsWithinRangeAction(): ResponseInterface
    {
        $startDateTimeObject = new DateTime($this->request->getArgument('rangeStart'));
        $endDateTimeObject = new DateTime($this->request->getArgument('rangeEnd'));
        $this->view->setVariablesToRender(['events']);
        $this->view->assign(
            'events',
            $this->entryRepository->findAllWithinRange($startDateTimeObject, $endDateTimeObject)
        );
        return $this->htmlResponse();
    }

    public function findSingleEventAction(): ResponseInterface
    {
        $eventUid = (int)$this->request->getArgument('event');
        $this->view->setVariablesToRender(['event']);
        $this->view->assign(
            'event',
            $this->entryRepository->findByEventUid($eventUid)
        );
        return $this->htmlResponse();
    }

    /**
     * @param EntryRepository $entryRepository
     */
    public function injectEntryRepository(EntryRepository $entryRepository): void
    {
        $this->entryRepository = $entryRepository;
    }
}
