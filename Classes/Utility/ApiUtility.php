<?php
declare(strict_types=1);

namespace JanHelke\CalendarApi\Utility;

use TYPO3\CMS\Extbase\Mvc\Request;

class ApiUtility
{

    /**
     * This function guesses the most likely action name based on parameters
     * transmitted in the request.
     *
     * &tx_calendarapi_entry[rangeStart]=2019-06-12T11:43:33&tx_calendarapi_entry[rangeEnd]=2019-08-12T11:43:33 => findEventsWithinRangeAction
     * &tx_calendarapi_entry[event]=123 => findSingleEventAction
     *
     * @param Request $request
     * @return string
     */
    public static function guessActionMethodNameBasedOnRequest(Request $request): string
    {
        if ($request->hasArgument('rangeStart') && $request->hasArgument('rangeEnd')) {
            return 'findEventsWithinRangeAction';
        }

        if ($request->hasArgument('event')) {
            return 'findSingleEventAction';
        }

        throw new \InvalidArgumentException("Can't guess the aimed action.", 1_563_258_053);
    }
}
