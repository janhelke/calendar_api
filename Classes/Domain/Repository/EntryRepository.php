<?php
declare(strict_types=1);

namespace JanHelke\CalendarApi\Domain\Repository;

use DateTime;
use Doctrine\DBAL\Types\Type;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Entry Repository
 */
class EntryRepository
{

    /**
     * @param DateTime $start
     * @param DateTime $end
     * @return array
     */
    public function findAllWithinRange(DateTime $start, DateTime $end): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_calendar_event');
        return $queryBuilder
            ->select(
                'event.uid as uid',
                'entry.uid as entry_uid',
                'event.start as start',
                'event.end as end',
                'entry.title as title',
                'entry.teaser as teaser'
            )
            ->from('tx_calendar_event', 'event')
            ->join('event', 'tx_calendar_entry', 'entry', 'event.entry_uid = entry.uid')
            ->where(
                $queryBuilder->expr()->gte(
                    'event.start',
                    $queryBuilder->createNamedParameter(
                        $connectionPool
                            ->getConnectionForTable('tx_calendar_event')
                            ->convertToDatabaseValue($start, Type::DATETIME)
                    )
                ),
                $queryBuilder->expr()->lte(
                    'event.end',
                    $queryBuilder->createNamedParameter(
                        $connectionPool
                            ->getConnectionForTable('tx_calendar_event')
                            ->convertToDatabaseValue($end, Type::DATETIME)
                    )
                )
            )
            ->execute()
            ->fetchAll();
    }

    /**
     * @param int $uid
     * @return mixed
     */
    public function findByEventUid(int $uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_event');
        return $queryBuilder
            ->select(
                '*',
                'event.uid as uid',
                'entry.uid as entry_uid',
                'event.start as start',
                'event.end as end'
            )
            ->from('tx_calendar_event', 'event')
            ->join('event', 'tx_calendar_entry', 'entry', 'event.entry_uid = entry.uid')
            ->where(
                $queryBuilder->expr()->eq('event.uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
            )
            ->execute()
            ->fetch();
    }

    /**
     * @param int $uid
     * @return mixed
     */
    public function findByUid(int $uid)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_entry');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_entry')
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
            )
            ->execute()
            ->fetch();
    }
}
