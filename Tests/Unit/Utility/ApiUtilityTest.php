<?php
declare(strict_types=1);

namespace JanHelke\CalendarApi\Tests\Unit\Utility;

use JanHelke\CalendarApi\Utility\ApiUtility;
use TYPO3\CMS\Extbase\Mvc\Request;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Entry Controller Test
 */
class ApiUtilityTest extends UnitTestCase
{

    /**
     * @return array
     */
    public function guessActionMethodNameBasedOnRequestDataProvider(): array
    {
        return [
            'findEventsWithinRangeAction' => [
                'requestArguments' => [
                    'rangeStart' => 'foo',
                    'rangeEnd' => 'bar'
                ],
                'expectedActionName' => 'findEventsWithinRangeAction'
            ],
            'findSingleEventAction' => [
                'requestArguments' => [
                    'event' => 123
                ],
                'expectedActionName' => 'findSingleEventAction'
            ]
        ];
    }

    /**
     * @dataProvider guessActionMethodNameBasedOnRequestDataProvider
     * @param array $requestArguments
     * @param string $expectedActionName
     */
    public function testGuessActionMethodNameBasedOnRequest(array $requestArguments, string $expectedActionName): void
    {
        $subject = new ApiUtility();
        $request = new Request();
        $request->setArguments($requestArguments);
        self::assertEquals(
            $expectedActionName,
            $subject::guessActionMethodNameBasedOnRequest($request)
        );
    }
}
