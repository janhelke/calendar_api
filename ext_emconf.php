<?php

$EM_CONF['calendar_api'] = [
    'title' => 'Calendar Api',
    'description' => 'A basic implementation of a working api for ext:cal to uncouple the calendar API from the view.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'calendar_foundation' => '1.0 - 1.999'
        ],
    ]
];
