<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

(static function () {
    ExtensionUtility::configurePlugin(
        'CalendarApi',
        'Entry',
        [
            \JanHelke\CalendarApi\Controller\EntryController::class => 'findEventsWithinRange, findSingleEvent',
        ],
        []
    );
})();
